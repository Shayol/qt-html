window.addEventListener('load', function () { 

    NodeList.prototype.forEach = Array.prototype.forEach;

    var page = document.querySelector(".page__container");
    var food = {'1': {title: "с фуа-гра", weight: "0.5", portions: "10", gift: "мышь в подарок", selected: "Фуа-гра просто класс!"},
                '2': {title: "с рыбой", weight: "2", portions: "40", gift: "<span class='cat-food__number'>2</span> мышей в подарок", selected: "Рыбка только из речки!"},
                '3': {title: "с курой", weight: "5", portions: "100", gift: "<span class='cat-food__number'>5</span> мышей в подарок<br>заказчик доволен.", selected: "Утка свежайшая"}
               };


    (function() {
        // page.innerHTML += `<p>$food['1'].title</p>`;
        for(var el in food) {
            page.innerHTML += `<div class="cat-food" data-id=${el}>
                                <div class="line"></div>
                                <div class="cat-food__header">
                                    <div class="cat-food__top-left-corner corner"></div>
                                    <div class="cat-food__top-phrase">Сказочное заморское яство</div>
                                </div>
                                <div class="cat-food__content">
                                    <div class="cat-food__brand">Нямушка</div>
                                    <div class="cat-food__title">${food[el].title}</div>
                                    <div class="cat-food__portions"><span class="cat-food__number">${food[el].portions}</span> порций</div>
                                    <div class="cat-food__mouse-gift">${food[el].gift}</div>
                                    <div class="cat-food__weight weight">
                                        <div class="weight__number">${food[el].weight}</div>
                                        <div class="weight__measurement-units">kg</div>
                                    </div>      
                                </div>
                                <div class="cat-food__bottom-phrase">Чего сидишь? Порадуй котэ,<a href="#" class="cat-food__buy"> купи.</a></div>     
                            </div>`;
        }
    })();

    var catFoodList = document.querySelectorAll(".cat-food__header, .cat-food__content, .cat-food__buy");

    catFoodList.forEach(el => el.addEventListener('click', (e) => {
       var parent = e.target.parentNode;

       while(!parent.classList.contains("cat-food")) {

            parent = parent.parentNode;

       }

       parent.classList.toggle("cat-food--selected");

       var bottomPhrase = parent.querySelector(".cat-food__bottom-phrase");
       var topPhrase = parent.querySelector(".cat-food__top-phrase");

       bottomPhrase.innerHTML = parent.classList.contains("cat-food--selected") ? food[parent.dataset.id].selected : 'Чего сидишь? Порадуй котэ,<a href="#" class="cat-food__buy"> купи.</a>';
       
       if(!parent.classList.contains("cat-food--selected")) {

           topPhrase.style.color = '#d91667';

           topPhrase.innerHTML = "Котэ не одобряет?";
       }

       else {
            topPhrase.innerHTML = 'Сказочное заморское яство';
            topPhrase.style.color = 'inherit';
       }
    
    })); 
    
    });